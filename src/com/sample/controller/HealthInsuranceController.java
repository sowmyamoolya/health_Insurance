package com.sample.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sample.model.UserDetails;
import com.sample.sevice.CalculatePremium;

@Controller
@RequestMapping("/")
public class HealthInsuranceController {
	@RequestMapping(value="/")
    public String sayHello(Model model) {
		model.addAttribute("user",new UserDetails());
		return "index";
	}
	@RequestMapping(value="/sendData",  method = RequestMethod.POST)
	 public String compute(Model model,@ModelAttribute("user")UserDetails details) {
		
		try {
			model.addAttribute("premium",CalculatePremium.getPremium(details));
			model.addAttribute("user",details);
		} catch (Exception e) {
			model.addAttribute("error","Please fill apropriate value...");
		}
		
		return "thanks";
	}

}
