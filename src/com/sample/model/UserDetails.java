package com.sample.model;

public class UserDetails {
private String prefix;
private String name;
private String age;
private String gender;
private String hypertension;
private String bp;
private String bloodSuger;
private String overweiht;
private String smoking;
private String alcohol;
private String exercise;
private String drugs;
@Override
public String toString() {
	return "UserDetails [userId=" + prefix + ", name=" + name + ", age=" + age
			+ ", gender=" + gender + ", Hypertension=" + hypertension + ", bp="
			+ bp + ", bloodSuger=" + bloodSuger + ", overweiht=" + overweiht
			+ ", smoking=" + smoking + ", alcohol=" + alcohol + ", exercise="
			+ exercise + ", drugs=" + drugs + "]";
}
public String getPrefix() {
	return prefix;
}
public void setPrefix(String userId) {
	this.prefix = userId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAge() {
	return age;
}
public void setAge(String age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getHypertension() {
	return hypertension;
}
public void setHypertension(String hypertension) {
	this.hypertension = hypertension;
}
public String getBp() {
	return bp;
}
public void setBp(String bp) {
	this.bp = bp;
}
public String getBloodSuger() {
	return bloodSuger;
}
public void setBloodSuger(String bloodSuger) {
	this.bloodSuger = bloodSuger;
}
public String getOverweiht() {
	return overweiht;
}
public void setOverweiht(String overweiht) {
	this.overweiht = overweiht;
}
public String getSmoking() {
	return smoking;
}
public void setSmoking(String smoking) {
	this.smoking = smoking;
}
public String getAlcohol() {
	return alcohol;
}
public void setAlcohol(String alcohol) {
	this.alcohol = alcohol;
}
public String getExercise() {
	return exercise;
}
public void setExercise(String exercise) {
	this.exercise = exercise;
}
public String getDrugs() {
	return drugs;
}
public void setDrugs(String drugs) {
	this.drugs = drugs;
}
public UserDetails() {
	super();
	// TODO Auto-generated constructor stub
}
public UserDetails(String userId, String name, String age, String gender,
		String hypertension, String bp, String bloodSuger, String overweiht,
		String smoking, String alcohol, String exercise, String drugs) {
	super();
	this.prefix = userId;
	this.name = name;
	this.age = age;
	this.gender = gender;
	this.hypertension = hypertension;
	this.bp = bp;
	this.bloodSuger = bloodSuger;
	this.overweiht = overweiht;
	this.smoking = smoking;
	this.alcohol = alcohol;
	this.exercise = exercise;
	this.drugs = drugs;
}

}
