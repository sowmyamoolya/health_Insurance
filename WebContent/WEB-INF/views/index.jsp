<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insurance</title>
</head>
<body>
Hello!!!

<form:form method = "POST" action = "./sendData" modelAttribute="user">
<table>
<tr>
               <td><form:label path = "prefix">Prefix</form:label></td>
               <td><form:select path = "prefix" >
               <form:option value="Mr.">Mr.</form:option>
               <form:option value="Ms">Ms.</form:option>
               <form:option value="Mrs.">Mrs.</form:option>
               </form:select>
               </td>
            </tr>
            <tr>
               <td><form:label path = "name">Name</form:label></td>
               <td><form:input path = "name" /></td>
            </tr>
            <tr>
               <td><form:label path = "age">Age</form:label></td>
               <td><form:input path = "age" /></td>
            </tr>
            <tr>
               <td><form:label path = "gender">Gender</form:label></td>
               <td><form:select path = "gender" >
               <form:option value="M">Male</form:option>
               <form:option value="F">Female</form:option>
               <form:option value="O">Other</form:option>
               </form:select>
               
               </td>
            </tr>
            
            <tr>
            <td colspan = "2">
            Current health:
            </td>
            </tr>
            <tr>
             <td><form:label path = "hypertension">Hypertension</form:label></td>
               <td><form:radiobutton path = "hypertension"  value="Y" /> Yes</td>
                <td><form:radiobutton path = "hypertension"  value="N" checked="checked" /> No</td>
            </tr>
            <tr>
             <td><form:label path = "bp">Blood Pressure</form:label></td>
               <td><form:radiobutton path = "bp"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "bp"  value="N" checked="checked" /> No</td>
            </tr>
            <tr>
             <td><form:label path = "bp">Blood Suger</form:label></td>
               <td><form:radiobutton path = "bloodSuger"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "bloodSuger"  value="N" checked="checked" /> No</td>
            </tr>
               <tr>
             <td><form:label path = "overweiht">overweiht</form:label></td>
               <td><form:radiobutton path = "overweiht"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "overweiht"  value="N" checked="checked" /> No</td>
            </tr>
              <tr>
            <td colspan = "2">
            Habits:
            </td>
            </tr>
            <tr>
             <td><form:label path = "smoking">smoking</form:label></td>
               <td><form:radiobutton path = "smoking"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "smoking"  value="N" checked="checked" /> No</td>
            </tr>
            <tr>
             <td><form:label path = "alcohol">alcohol</form:label></td>
               <td><form:radiobutton path = "alcohol"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "alcohol"  value="N" checked="checked" /> No</td>
            </tr>
            <tr>
             <td><form:label path = "exercise">exercise</form:label></td>
               <td><form:radiobutton path = "exercise"  value="Y" checked="checked" /> Yes</td>
                <td><form:radiobutton path = "exercise"  value="N" /> No</td>
            </tr>
            <tr>
             <td><form:label path = "drugs">drugs</form:label></td>
               <td><form:radiobutton path = "drugs"  value="Y"/> Yes</td>
                <td><form:radiobutton path = "drugs"  value="N" checked="checked" /> No</td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
         </table>  

</form:form>


</body>
</html>